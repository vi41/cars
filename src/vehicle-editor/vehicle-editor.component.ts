import {Component} from '@angular/core';
import './vehicle-editor.component.less';
import {LoginService} from "src/services/login.service";

@Component({
  selector: '.vehicle-editor',
  templateUrl: './vehicle-editor.component.html'
})
export class VehicleEditorComponent {
  constructor(private loginService: LoginService){};

  getUserName(): string {
    return this.loginService.getUserName();
  }

  isLoggedIn() : boolean {
    return this.loginService.isLoggedIn();
  }

  logout() {
    this.loginService.logout();
  }

}