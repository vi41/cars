var express = require('express');
var fs = require('fs');
var https = require('https');
var app = express();
var env = require('./env.js');

app.use(express.static(__dirname + '/build'));


/* Load https certificates */
var key = fs.readFileSync(env.priv_key, 'utf8');
var cert = fs.readFileSync(env.pub_key, 'utf8');

var https_options = {
  key: key,
  cert: cert
};

/* Start https server */

https.createServer(https_options, app).listen(env.port, env.url);

console.info("Server is running on port: " + env.port);