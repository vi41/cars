To run this project you need:
 1. Install Node.js platform, npm command should be available after that
 2. Run:
   - 'npm run install-start-prod' to build and install prod version
   - 'npm run install-start-dev' to build and install dev version
 3. After you will see message 'Server is running on port: 443' you can input in your browser this url: https://localhost
 4. If you need to change server port, you can do it in ./env.js file