import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {AppComponent} from "./app.component";
import {VehicleEditorModule} from "./vehicle-editor/vehicle-editor.module";
import {VehicleEditorComponent} from "./vehicle-editor/vehicle-editor.component";

const appRoutes: Routes = [
  {path: '', component: VehicleEditorComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true}),
    FormsModule,
    VehicleEditorModule
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent],
})
export class AppModule {

}
