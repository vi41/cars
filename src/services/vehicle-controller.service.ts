import {Injectable} from "@angular/core";

@Injectable()
export class VehicleControllerService {

  public static MODEL: any = {
    SOPHIA: "sophia"
  };

  public static FRONT: any = {
    FRONT1: "front1",
    FRONT2: "front2"
  };

  public static SCALE: any = {
    SCALE_1_43: "1:43",
    SCALE_1_24: "1:24",
    SCALE_1_18: "1:18",
  };

  public static TOP: any = {
    TOP1: "top1",
    TOP2: "top2"
  };

  public static SIDE: any = {
    SIDE1: "side1",
    SIDE2: "side2"
  };

  public static BACKSIDE: any = {
    BACKSIDE1: "backside1",
    BACKSIDE2: "backside2"
  };

  public static ENGINE: any = {
    ENGINE1: "engine1",
    ENGINE2: "engine2",
  };

  public static RIM: any = {
    RIM1: "rim1",
    RIM2: "rim2",
  };

  public static MATERIAL: any = {
    FLUXMO_PREMIUM: "fluxmo_premium",
    FLUXMO_SUPERIOR: "fluxmo_superior",
  };

  public static TEXT: any = {
    TEXT1: "text1",
    TEXT2: "text2",
  };


  private static logChanges(feature: string, value: any) {
    console.info(`${feature} changed:`, value)
  }

  public setModel(model: any) {
    VehicleControllerService.logChanges("Model", model);
  }

  public setFront(front: string) {
    VehicleControllerService.logChanges("Front", front);
  }

  public setScale(scale: string) {
    VehicleControllerService.logChanges("Scale", scale);
  }

  public setTop(top: string) {
    VehicleControllerService.logChanges("Top", top);
  }

  public setSide(side: string) {
    VehicleControllerService.logChanges("Side", side);
  }

  public setBackside(backside: string){
    VehicleControllerService.logChanges("Backside", backside);
  }

  public setEngine(engine: string) {
    VehicleControllerService.logChanges("Engine", engine);
  }

  public setRim(rim: string){
    VehicleControllerService.logChanges("Rim", rim);
  }

  public setMaterial(material: string){
    VehicleControllerService.logChanges("Material", material);
  }

  public setText(text: string){
    VehicleControllerService.logChanges("Text", text);
  }
}
