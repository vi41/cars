export interface MenuItem {
  header?: string;
  icon?: string;
  selected?:boolean;
  action?(MenuItem);
  childItems?: MenuItem[];
  indentPx?: number;
}
