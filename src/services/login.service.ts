import {Injectable} from "@angular/core";

@Injectable()
export class LoginService {

  isLoggedIn(): boolean {
    return true;
  }

  logout() {
    console.info("Log out");
  }

  getUserName(): string {
    return "Fluxmann";
  }
}
