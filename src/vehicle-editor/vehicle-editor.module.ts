import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule}  from '@angular/platform-browser';
import {AccordionModule} from "primeng/components/accordion/accordion";
import {LoginService} from "src/services/login.service";
import {VehicleToolbarComponent} from "src/vehicle-editor/toolbar/vehicle-toolbar.component";
import {VehicleEditorComponent} from "src/vehicle-editor/vehicle-editor.component";
import {VehicleViewComponent} from "src/vehicle-editor/vehicle-view/vehicle-view.component";
import {VehiclePaletteComponent} from "src/vehicle-editor/palette/vehicle-palette.component";
import {VehicleMenuComponent} from "src/vehicle-editor/menu/vehicle-menu.component";
import {VehicleControllerService} from "src/services/vehicle-controller.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AccordionModule
  ],
  declarations: [
    VehicleEditorComponent,
    VehicleViewComponent,
    VehiclePaletteComponent,
    VehicleMenuComponent,
    VehicleToolbarComponent
  ],
  exports: [
    VehicleEditorComponent
  ],
  providers: [
    LoginService,
    VehicleControllerService
  ],
  bootstrap: [VehicleEditorComponent]
})
export class VehicleEditorModule {
}