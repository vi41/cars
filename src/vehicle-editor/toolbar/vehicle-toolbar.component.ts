import {Component} from '@angular/core';
import './vehicle-toolbar.component.less';
import {MenuItem} from "src/vehicle-editor/menu-item";

const UPLOAD_IMG = require('src/assets/img/vehicle-editor/upload.png');
const DOWNLOAD_IMG = require('src/assets/img/vehicle-editor/download.png');
const SHARE_IMG = require('src/assets/img/vehicle-editor/share.png');

@Component({
  selector: '.vehicle-toolbar',
  templateUrl: './vehicle-toolbar.component.html'
})
export class VehicleToolbarComponent {
  items: Array<MenuItem> = [
    {
      icon: UPLOAD_IMG,
      action: () => console.info("Upload")
    },
    {
      icon: DOWNLOAD_IMG,
      action: () => console.info("Download")
    },
    {
      icon: SHARE_IMG,
      action: () => console.info("Share")
    }
  ];
}