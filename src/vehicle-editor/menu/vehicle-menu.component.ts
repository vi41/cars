import {Component} from "@angular/core";
import "./vehicle-menu.component.less";
import {MenuItem} from "src/vehicle-editor/menu-item";
import * as _ from 'lodash';
import {VehicleControllerService} from "src/services/vehicle-controller.service";

const MODEL_SOPHIA_IMG = require('src/assets/img/vehicle-editor/model_sophia.png');
const COMING_SOON_IMG = require('src/assets/img/vehicle-editor/coming_soon.png');
const FRONT1_IMG = require('src/assets/img/vehicle-editor/front1.png');
const FRONT2_IMG = require('src/assets/img/vehicle-editor/front2.png');
const TOP1_IMG = require('src/assets/img/vehicle-editor/top1.png');
const TOP2_IMG = require('src/assets/img/vehicle-editor/top2.png');
const SIDE1_IMG = require('src/assets/img/vehicle-editor/side1.png');
const SIDE2_IMG = require('src/assets/img/vehicle-editor/side2.png');
const BACKSIDE1_IMG = require('src/assets/img/vehicle-editor/backside1.png');
const BACKSIDE2_IMG = require('src/assets/img/vehicle-editor/backside2.png');
const ENGINE1_IMG = require('src/assets/img/vehicle-editor/engine1.png');
const ENGINE2_IMG = require('src/assets/img/vehicle-editor/engine2.png');
const RIM1_IMG = require('src/assets/img/vehicle-editor/rim1.png');
const RIM2_IMG = require('src/assets/img/vehicle-editor/rim2.png');
const TEXT1_IMG = require('src/assets/img/vehicle-editor/text1.png');
const TEXT2_IMG = require('src/assets/img/vehicle-editor/text2.png');


@Component({
  selector: '.vehicle-menu',
  templateUrl: './vehicle-menu.component.html'
})
export class VehicleMenuComponent {
  private static COMING_SOON_MENU_ITEM: MenuItem = {
    icon: COMING_SOON_IMG
  };

  menuItems: MenuItem[] = [
    {
      header: 'Modell',
      childItems: [
        {
          icon: MODEL_SOPHIA_IMG,
          action: () => this.vehicleController.setModel(VehicleControllerService.MODEL.SOPHIA),
          selected: true
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Front',
      childItems: [
        {
          icon: FRONT1_IMG,
          action: () => this.vehicleController.setFront(VehicleControllerService.FRONT.FRONT1),
          selected: true
        },
        {
          icon: FRONT2_IMG,
          action: () => this.vehicleController.setFront(VehicleControllerService.FRONT.FRONT2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Top',
      childItems: [
        {
          icon: TOP1_IMG,
          action: () => this.vehicleController.setTop(VehicleControllerService.TOP.TOP1),
          selected: true
        },
        {
          icon: TOP2_IMG,
          action: () => this.vehicleController.setTop(VehicleControllerService.TOP.TOP2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Seite',
      childItems: [
        {
          icon: SIDE1_IMG,
          action: () => this.vehicleController.setSide(VehicleControllerService.SIDE.SIDE1),
          selected: true
        },
        {
          icon: SIDE2_IMG,
          action: () => this.vehicleController.setSide(VehicleControllerService.SIDE.SIDE2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Heck',
      childItems: [
        {
          icon: BACKSIDE1_IMG,
          action: () => this.vehicleController.setBackside(VehicleControllerService.BACKSIDE.BACKSIDE1),
          selected: true
        },
        {
          icon: BACKSIDE2_IMG,
          action: () => this.vehicleController.setBackside(VehicleControllerService.BACKSIDE.BACKSIDE2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Motor',
      childItems: [
        {
          icon: ENGINE1_IMG,
          action: () => this.vehicleController.setEngine(VehicleControllerService.ENGINE.ENGINE1),
          selected: true
        },
        {
          icon: ENGINE2_IMG,
          action: () => this.vehicleController.setEngine(VehicleControllerService.ENGINE.ENGINE2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Felgen',
      childItems: [
        {
          icon: RIM1_IMG,
          action: () => this.vehicleController.setRim(VehicleControllerService.RIM.RIM1),
          selected: true
        },
        {
          icon: RIM2_IMG,
          action: () => this.vehicleController.setRim(VehicleControllerService.RIM.RIM2)
        },
        VehicleMenuComponent.COMING_SOON_MENU_ITEM
      ]
    },
    {
      header: 'Material',
      childItems: [
        {
          header: 'Fluxmo Premium',
          action: () => this.vehicleController.setMaterial(VehicleControllerService.MATERIAL.FLUXMO_PREMIUM),
          selected: true
        },
        {
          header: 'Fluxmo Superior',
          action: () => this.vehicleController.setMaterial(VehicleControllerService.MATERIAL.FLUXMO_SUPERIOR)
        }
      ]
    },
    {
      header: 'Text',
      childItems: [
        {
          icon: TEXT1_IMG,
          action: () => this.vehicleController.setText(VehicleControllerService.TEXT.TEXT1),
          indentPx: 0,
          selected: true
        },
        {
          icon: TEXT2_IMG,
          indentPx: 0,
          action: () => this.vehicleController.setText(VehicleControllerService.TEXT.TEXT2)
        }
      ]
    },
    {
      header: 'Maßstab',
      childItems: [
        {
          header: VehicleControllerService.SCALE.SCALE_1_43,
          action: () => this.vehicleController.setScale(VehicleControllerService.SCALE.SCALE_1_43),
          selected: true
        },
        {
          header: VehicleControllerService.SCALE.SCALE_1_24,
          action: () => this.vehicleController.setScale(VehicleControllerService.SCALE.SCALE_1_24),
        },
        {
          header: VehicleControllerService.SCALE.SCALE_1_18,
          action: () => this.vehicleController.setScale(VehicleControllerService.SCALE.SCALE_1_18),
        }
      ]
    },
    {
      header: 'Display',
      childItems: [
        {
          header: 'Display +',
        }
      ]
    },
  ];

  constructor(private vehicleController: VehicleControllerService) {
    this.init();
  };

  private init() {
    _.forEach(this.menuItems, (parent) => {
      _.forEach(parent.childItems, (child) => {
        if(typeof child.indentPx === 'undefined'){
          child.indentPx = 10;
        }
      });
    });
  }

  action(parent: MenuItem, child: MenuItem) {
    if (child.action) {
      parent.childItems && _.forEach(parent.childItems, (item) => {
        item.selected = false
      });
      child.selected = true;
      child.action(child);
    }
  }

  isActive(item: MenuItem): boolean {
    return item.action && item.selected
  }


}