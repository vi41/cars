import {Component} from '@angular/core';
import './vehicle-palette.component.less';

@Component({
  selector: '.vehicle-palette',
  templateUrl: './vehicle-palette.component.html'
})

export class VehiclePaletteComponent {
  SHADOW_TYPE: any = {
    WHITE: "WHITE",
    BLACK: "BLACK"
  };


  colorButtons: Array<Object> = [
    {
      color: "#000000",
      shadow: this.SHADOW_TYPE.WHITE
    },
    {
      color: "#ffffff",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#d1d2d4",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#91bf4a",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#fbae3d",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#ee3e35",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#90268f",
      shadow: this.SHADOW_TYPE.BLACK
    },
    {
      color: "#1074bc",
      shadow: this.SHADOW_TYPE.BLACK
    }
  ];

  selectedColor: String = null;

  select(color: string) {
    this.selectedColor = color;
  }
}